﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraFollow : MonoBehaviour {

    /*private Vector2 velocity;
    public float smoothTimeY;
    public float smoothTimeX;
    public GameObject player;

    void Start () {

        player = GameObject.FindGameObjectsWithTag("Player");
	}
	
    void FIdesUpdate()
    {
        float posX = Mathf.SmoothDamp(transform.position, player.transform.position.x, ref velocity.x, smoothTimeX);
        float posY = Mathf.SmoothDamp(transform.position, player.transform.position.y, ref velocity.y, smoothTimeY);

        transform.position = new Vector3(posX, posY, transform.position.z);
    }*/
    //[SerializeField] = To make the private variable visible in the inspector.
    //private float xMax;
  
    public Transform target;
    public float smoothing;

    private Vector3 offset;

    private float lowY;

    void Start() {
        offset = transform.position - target.position;
        lowY = transform.position.y;
        //target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void LateUpdate()
    {
        Vector3 targetCamPos = target.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing*Time.deltaTime);

        if (transform.position.y < lowY) transform.position = new Vector3(transform.position.x, lowY, transform.position.z);
        //Vector3(0,0,0)
        //transform.position = new Vector3(target.position.x, target.position.y + yOffset, transform.position.z);
    }
}
