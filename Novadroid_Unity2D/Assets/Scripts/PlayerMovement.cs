﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Animator anim;
    public AudioClip playerSteps;
    AudioSource footsteps;

    public bool canMove;

    void Start()
    {
        anim = GetComponent<Animator>();

        footsteps = GetComponent<AudioSource>();
    }

    void Update()
    {

        Movement();

        float move = Input.GetAxis("Horizontal");
        //Debug.Log(move);
        //move = 10f;
        anim.SetFloat("Speed", move);

    }

    void Movement()
    {
        if (!canMove)
        {
            return;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector2.right * 3f * Time.deltaTime);
            transform.eulerAngles = new Vector2(0, 0);
            //StepsSound();
            //footsteps.Play();
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //transform.Translate(Vector2.left * 3f * Time.deltaTime);
            transform.Translate(-Vector2.right * 3f * Time.deltaTime);
            transform.eulerAngles = new Vector2(0, 0);
            //footsteps.Play();
        }
        
    }

    /*void StepsSound()
    {
        footsteps.clip = playerSteps;      
        footsteps.Play(2000);
    }*/

    
}
