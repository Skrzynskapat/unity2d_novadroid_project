﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateTextAtLine : MonoBehaviour {

    public TextAsset theText;   

    public int startLine;
    public int endAtLine;

    public TextBoxManager theTextBox; 

    public bool destroyWhenActivated;


	// Use this for initialization
	void Start () {
        theTextBox = FindObjectOfType<TextBoxManager>();

        

    }
	
	// Update is called once per frame
	void Update () {
		/*if (endAtLine == 8)
        {
			this..GetComponent<Collider2D>().enabled = false;
        }*/
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
		if (other.name == "Player77" && theTextBox.endLine != endAtLine )
        {
            theTextBox.ReloadScript(theText);
            theTextBox.currentLine = startLine;
            theTextBox.endLine = endAtLine;
            theTextBox.EnableTextBox();      

            /*if (destroyWhenActivated)
            {
                Destroy(gameObject);
            }*/
        }
    }

    /*void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "romeo")
        {
            collider75.GetComponent<Collider2D>().enabled = false;
        }
    }*/


}
